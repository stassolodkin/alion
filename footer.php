<?php

/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 17/04/2017
 * Time: 6:19 PM
 */
class footer
{

    public static function render()
    {
        echo '
        <footer>
        
            <!--<div id="colophon" class="wrapper clearfix">
                ALION SOLUTIONS - EXCELLENCE FIRST
            </div>-->
    
            <div
                id="attribution"
                class="wrapper clearfix"
                style="font-size:11px;"
            >
                <div">
                    Alion Solutions &copy; 2017
                </div>
    
            </div>
        </footer>
        ';
    }
}
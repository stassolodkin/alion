<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Continuous Integration And Delivery</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <?php
        require('../head.php');
        head::renderExternalResources();
    ?>

</head>

<body id="home">

<!-- header area -->
<header class="wrapper clearfix">

    <?php
        require('../logo.php');
        logo::render();

        require('../nav.php');
        menu::renderMenu(menu::SERVICES);
    ?>
</header>
<!-- end header -->
 
<section id="page-header" class="clearfix">    

    <div class="wrapper">
	    <h1>Continuous Integration And Delivery</h1>
    </div>

</section>


<div class="wrapper" id="main"> 

	<section id="content2">

        <h2>Continuous Integration And Delivery</h2>
        <p style="text-align:justify;">
            <p>Alion Solutions is a Melbourne-based software development and IT consultancy firm. We have emerged from a group of individuals that come from a wide variety of information technology-related areas ranging from hardware and software support, through application development to IT project management.</p>
            <p>We specialise in creation of online applications and assisting software companies in modernising their tools and processes in order to keep up with the ever evolving industry.</p>
        </p>
    </section>

</div>
    

<?php
    require('../footer.php');
    footer::render();
?>


<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="/js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="/js/main.js"></script>

</body>
</html>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Alion - Services</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <?php
        require('head.php');
        head::renderExternalResources();
    ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body id="home">

<!-- header area -->
<header class="wrapper clearfix">

    <?php
        require('logo.php');
        logo::render();

        require('nav.php');
        menu::renderMenu(menu::CONTACT);
    ?>
</header>
<!-- end header -->
 
<section id="page-header" class="clearfix">    

    <div class="wrapper">
	    <h1>Contact Details</h1>
    </div>

</section>


<div class="wrapper" id="main">

    <section>
        <h2>Enquire</h2>

        <form method="POST" class="form">

            <p class="name">
                <label for="name">Name</label><br />
                <input type="text" name="email_name" id="email_name" />
            </p>

            <p class="email">
                <label for="email">Email</label><br />
                <input type="text" name="email_from" id="email_from" />
            </p>

            <p class="subject">
                <label for="web">Subject</label><br />
                <input type="text" name="email_subject" id="email_subject" />
            </p>

            <p class="message">
                <textarea name="email_body" id='email_body' placeholder="Message" /></textarea>
            </p>
            
            <p id="email_success" style="color: green; display: none;"></p>
            <p id="email_error" style="color: darkred; display: none;"></p>

<!--            <p class="submit">-->
<!--                <input type="submit" value="Send" />-->
<!--            </p>-->

            <div class="g-recaptcha" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>
            <br />
            <input type="button" class="send-email" value="Send" onclick="sendEmail()">
        </form>
    </section>

    <section>

        <h2>Contact details</h2>
        <p>
            Address: 3/21 Anderson St, Caulfield  Vic 3162.<br/>
            Ph:      03 99729846<br/>
            Email:   info@alion.com.au<br/>
        </p>
    </section>

</div>
    

<?php
    require('footer.php');
    footer::render();
?>


<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="js/main.js"></script>

</body>
</html>
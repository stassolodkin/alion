<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Alion - Services</title>

    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <?php
        require('head.php');
        head::renderExternalResources();
    ?>

</head>

<body id="home">

<!-- header area -->
<header class="wrapper clearfix">

    <?php
        require('logo.php');
        logo::render();

        require('nav.php');
        menu::renderMenu(menu::SERVICES);
    ?>
</header>
<!-- end header -->
 
<section id="page-header" class="clearfix">    
<!-- responsive FlexSlider image slideshow -->
<div class="wrapper">
	<h1>Services</h1>
    </div>

</section>


<!-- main content area -->   
<div class="wrapper" id="main"> 
    
<!-- content area -->    
<!--	<section id="content">-->

        <h2>Software development</h2>
        <p style="text-align:justify;">
            We have a wide range of experience, from static responsive websites to high-volume, real-time, mission-critical, systems that have no right for error, i.e.: Mobile Prepaid Billing.
        </p>

        <h2>Service Hosting</h2>
        <p style="text-align:justify;">
            We can host a variety of services, from Web, Email and DNS to any custom application.
        </p>


        <h2>Consulting</h2>
        <h3>Software companies</h3>
        <p style="text-align:justify;">
            Aiding software development companies to setup and maintain modern practices.
        </p>

        <h3>Everyone</h3>
        <p style="text-align:justify;">Design, implementation and maintenance of IT infrastructure.</p>

        <h2>Process Improvement</h2>
        <h3>Agile</h3>
        <p style="text-align:justify;">
            We can either analyse the applicability of Agile methodology to your existing processes and help upgrade them as necessary or design and help implementing the processes from scratch.
        </p>

        <h3>Development Environment</h3>
        <p style="text-align:justify;">
            We can design your development environment and process and help automating the process of building these environments for new developers.
        </p>

        <h2>Infrastructure</h2>
        <p style="text-align:justify;">
            We can design, implement and maintain infrastructure for your production, testing or developmet environments.
        </p>


        <h2>Automated Testing</h2>
        <p>
            Automated testing as well as test-driven development has proved to be invaluable in the industry, but has still not been adopted by a lot of software development companies.
            We can assist in designing the process and/or building the automated test suite for you.
        </p>


        <h2>Continuous Integration and Delivery</h2>
        <p>
            Continuous integration and delivery are processes by which your automated tests are run and software is deployed automatically whenever a developer commits a change to your repository.
            We can set this up and host this for you.
        </p>


<!--    </section>-->
      
    <!-- sidebar -->    
<!--    <aside>-->
<!--        <h3>Secondary Section menu</h3>-->
<!--            <nav id="secondary-navigation">-->
<!--                    <ul>-->
<!--                        <li><a href="#">menu item</a></li>-->
<!--                        <li class="current"><a href="#">current menu item</a></li>-->
<!--                        <li><a href="#">menu item</a></li>-->
<!--                        <li><a href="#">menu item</a></li>-->
<!--                        <li><a href="#">menu item</a></li>-->
<!--                    </ul>-->
<!--             </nav>-->
<!--      </aside>-->
   
  </div>
    

<?php
    require('footer.php');
    footer::render();
?>


<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="js/main.js"></script>

</body>
</html>
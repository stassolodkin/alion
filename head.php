<?php

/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 17/04/2017
 * Time: 6:19 PM
 */
class head
{

    public static function renderExternalResources()
    {
        echo '
            <link rel="shortcut icon" href="/images/favicon.ico"  type="image/x-icon">
    
            <!-- CSS-->
            <!-- Google web fonts. You can get your own bundle at http://www.google.com/fonts. Don\'t forget to update the CSS accordingly!-->
            <link href=\'http://fonts.googleapis.com/css?family=Droid+Serif|Ubuntu\' rel=\'stylesheet\' type=\'text/css\'>
            <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
            
            <link rel="stylesheet" href="/css/normalize.css">
            <link rel="stylesheet" href="/js/flexslider/flexslider.css">
            <link rel="stylesheet" href="/css/styles.css">
        
            <!-- end CSS-->
        
            <!-- JS-->
            <script src="/js/libs/modernizr-2.6.2.min.js"></script>
            <!-- end JS-->
        ';
    }
}
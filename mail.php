<?php

/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 17/04/2017
 * Time: 6:19 PM
 */

require_once __DIR__ . '/bootstrap.php';

//$logger->info("Another INFO message");
//$logger->error("just another ERROR message");

$returnArray = [];

if(!empty($_POST['emailName'])) {
    $name = checkInput($_POST['emailName']);
}

if(!empty($_POST['emailFrom'])) {
    $email = checkInput($_POST['emailFrom']);
}

if(!empty($_POST['emailSubject'])) {
    $subject = checkInput($_POST['emailSubject']);
}

if(!empty($_POST['emailBody'])) {
    $message = checkInput($_POST['emailBody']);
}

if (!empty($_POST['gRecaptureResponse'])) {
    $captcha = $_POST['gRecaptureResponse'];

    $secret = $config->get('recapture_secret');;
    $reCaptureValidation = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=".$_SERVER['REMOTE_ADDR']), true);

    if(empty($reCaptureValidation['success'])) {
        failReCapture();
        exit();
    }
} else {
    failReCapture();
    exit();
}


if(!empty($name) && !empty($email) && !empty($subject) && !empty($message)) {

    $recipient = $config->get('from_email');
    $mailHeader = "From: $email \r\n";

    $sent = mail($recipient, $subject, $message, $mailHeader);

    if($sent) {
        $returnArray['email_success'] = "Your email has been sent to Alion Solutions.";

    } else {
        $returnArray['email_error'] = "There was an error sending the email. Please try again later.";
    }
} else {

    $returnArray['email_error'] = "Please fill all fields.";
}

header('Content-Type: application/json');
echo json_encode($returnArray);

function failReCapture() {
    $returnArray['email_error'] = "Capture verification failed!";
    header('Content-Type: application/json');
    header('X-PHP-Response-Code: 500', true, 500);
    echo json_encode($returnArray);
}

function checkInput($data) {

    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


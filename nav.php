<?php

class menu {

    const INDEX      = 1;
    const SERVICES   = 2;
    const ABOUT      = 3;
    const CONTACT    = 4;

    static public function renderMenu($currentPage = menu::INDEX) {

        echo  '
            <nav id="topnav" role="navigation">
                <div class="menu-toggle">Menu</div>
                <ul class="srt-menu" id="menu-main-navigation">';


        echo '<li ';
        if ($currentPage == menu::INDEX) {
            echo 'class="current"';
        }
        echo '><a href="/index.php">Home</a></li>';


        echo '<li ';
        if($currentPage == menu::SERVICES) {
            echo 'class="current"';
        }
        echo '><a href="/services.php">Services</a>
            <ul>
                <li><a href="/services/software_development.php">Software Development</a></li>
                <li><a href="/services/hosting.php">Hosting</a></li>
                <li><a href="/services/consulting.php">Consulting</a></li>
                <li><a href="/services/process_improvement.php">Process Improvement</a></li>
                <li><a href="/services/development_environment.php">Development Environment</a></li>
                <li><a href="/services/infrastructure.php">Infrastructure</a></li>
                <li><a href="/services/automated_testing.php">Automated Testing</a></li>
                <li><a href="/services/continuous_integration.php">Continuous Integration</a></li>
            </ul>
        </li>';

        echo '<li ';
        if($currentPage == menu::ABOUT) {
            echo 'class="current"';
        }
        echo '><a href="/about.php">About Us</a></li>';

        echo '<li ';
        if($currentPage == menu::CONTACT) {
            echo 'class="current"';
        }
        echo '><a href="/contact.php">Contact</a></li>';


        echo '
                </ul>
            </nav>';
    }
}

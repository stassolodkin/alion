<?php

require_once __DIR__ . '/vendor/autoload.php';


use Noodlehaus\Config;

$config = new Config(__DIR__ . '/config');

/**
 * Logger
 */
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$logger = new Logger('general');

$browserHandler = new BrowserConsoleHandler(Logger::INFO);
////$streamHandler = new StreamHandler('php://stderr', Logger::ERROR);
$streamHandler = new StreamHandler($config->get('log_file'), Logger::ERROR);

$logger->pushHandler($browserHandler);
$logger->pushHandler($streamHandler);
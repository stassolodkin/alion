<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Alion Solutions</title>
    <meta name="description" content="Simple Template">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">


    <?php
        require('head.php');
        head::renderExternalResources();
    ?>

</head>

<body id="home">
  

    <header class="wrapper clearfix">

        <?php
            require('logo.php');
            logo::render();

            require('nav.php');
            menu::renderMenu(menu::INDEX);
        ?>
    </header>

<!--    <section id="hero" class="clearfix">-->

    <div class="flexslider">
        <div>
            <img width="100%" src="images/slice_1.jpg" />
        </div>
    </div>

<!--    <div class="wrapper">-->


<!--        <div class="row">-->
<!--            <div >-->
<!--                <div class="flexslider">-->
<!--                    <ul class="slides">-->
<!--                        <li>-->
<!--                            <img src="images/basic-pic2.jpg" />-->
<!--                            <p class="flex-caption">Love Brazil !!! Sea view from Rio de Janeiro fort.</p>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="http://www.prowebdesign.ro"><img src="images/basic-pic2.jpg" /></a>-->
<!--                            <p class="flex-caption">El Arco Cabo Mexico. This image is wrapped in a link.</p>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <img src="images/basic-pic3.jpg" />-->
<!--                            <p class="flex-caption">Arches National Park, Utah, Usa.</p>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <img src="images/basic-pic4.jpg" />-->
<!--                            <p class="flex-caption">Morocco desert.</p>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->



<!--       </div>-->
<!--    </section>-->





<!-- main content area -->   
<div id="main" class="wrapper">
    <div class="grid_12">
        <h1>The need to be ahead</h1>
        <p style="text-align:justify;">
            As we all know, today, new technologies emerge every day, standards change rapidly and the rate of this change increases
            exponentially. Software development is a very involved process that requires a lot of attention and it is common for a
            software company to find itself using old tools and processes that are well behind the current standards before anyone
            has a chance to attend to this.
        </p>
        <p style="text-align:justify;">
            Sooner or later the result of neglecting the ever evolving industry tools will render your software unmaintainable if not
            unusable and will require rewriting it from scratch or going through a lengthy and tedious series of upgrades.
        </p>
        <p style="text-align:justify;">
            We specialise in helping comanies that do not have required resources to attent to these matters and analyse where it
            currently stands within this realm as well as designing the strategy and, if required, implementing the process
            of moving the required components to a more modern version or a counterpart.
        </p>
        <p style="text-align:justify;">
            Please read more about our services by following the link below.
        </p>

        <a href="#" class="buttonlink">Services</a>
    </div>

<!--	<section id="content" class="wide-content">-->
<!--      <div class="row">	-->
<!--        <div class="grid_4">-->
<!--        	<h1 class="first-header">Brazil!</h1>-->
<!--            <img src="images/basic-pic1.jpg" />-->
<!--            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>-->
<!--        </div>-->
<!--        -->
<!--        <div class="grid_4">-->
<!--        	<h1 class="first-header">Mexico!</h1>-->
<!--            <img src="images/basic-pic2.jpg" />-->
<!--            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>-->
<!--        </div>-->
<!--        -->
<!--        <div class="grid_4">-->
<!--        	<h1 class="first-header">US!</h1>-->
<!--            <img src="images/basic-pic3.jpg" />-->
<!--            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>-->
<!--        </div>-->
<!--	  </div>-->
<!--	</section> -->

    
      
  </div>

    <?php
        require('footer.php');
        footer::render();
    ?>


<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="js/main.js"></script>

</body>
</html>

$(document).ready(function(){
	
	// initialise  slideshow
	 $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });

});

function sendEmail() {

	$('#email_success').hide();
	$('#email_error').hide();

	var emailName            = $('#email_name').val();
	var emailFrom            = $('#email_from').val();
	var emailSubject         = $('#email_subject').val();
	var emailBody            = $('#email_body').val();
	var gRecaptureResponse   =  grecaptcha.getResponse();

	// Validation here

	var data = {
		emailName:           emailName,
		emailFrom:           emailFrom,
		emailSubject:        emailSubject,
		emailBody:           emailBody,
		gRecaptureResponse:  gRecaptureResponse
	};

	$.ajax({
		type: "POST",
		url: "mail.php",
		data: data,
		success: function (data) {
			for(var key in data) {
				$('#' + key).text(data[key]).fadeIn(1000);
				setTimeout(function() {
					$('#' + key).fadeOut();
				}, 4000);
			}
		},
		error: function (data) {

			var ajaxError = JSON.parse(data.responseText);

			$('#email_error').text(ajaxError.email_error).fadeIn(1000);
			setTimeout(function() {
				$('#email_error').fadeOut();
			}, 4000);
		}
	});
}
/**
 * Handles toggling the navigation menu for small screens.
 */
( function() {
	var button = document.getElementById( 'topnav' ).getElementsByTagName( 'div' )[0],
	    menu   = document.getElementById( 'topnav' ).getElementsByTagName( 'ul' )[0];

	if ( undefined === button )
		return false;

	// Hide button if menu is missing or empty.
	if ( undefined === menu || ! menu.childNodes.length ) {
		button.style.display = 'none';
		return false;
	}

	button.onclick = function() {
		if ( -1 == menu.className.indexOf( 'srt-menu' ) )
			menu.className = 'srt-menu';

		if ( -1 != button.className.indexOf( 'toggled-on' ) ) {
			button.className = button.className.replace( ' toggled-on', '' );
			menu.className = menu.className.replace( ' toggled-on', '' );
		} else {
			button.className += ' toggled-on';
			menu.className += ' toggled-on';
		}
	};
} )();
